
public class CasePion {
    
    private char PionPosition;
    private int  LignePion;
    private int  EtatCasePion;
     
//   Constructeur sans parametre  et avec parametre
    public CasePion()
    {
      PionPosition = ' ';
      LignePion  =  0; 
      EtatCasePion = 0;
    } 
    
    
    public CasePion( char newPionPosition,  int newLignePion)
    {
      PionPosition = newPionPosition;
      LignePion  =  newLignePion;  
    }
    
//    Methode de gestion des CasePion
    public void  setPionPosition(char newPionPosition)  	// La colonne de la Case
    {
        PionPosition = newPionPosition;     
    }

    
    public void setLignePion( int newLignePion)				// La ligne de la Case
    {
          LignePion = newLignePion;
    }
      
    
    public void setAllPosition( char newPionPosition, int newLignePion) 		// LA colonne et la Ligne de la Case
    {
      PionPosition =  newPionPosition;
      LignePion  =  newLignePion;  
    }

//    Gestion de l'etat de la Case
    public void  setEtatCasePion(int newEtatCasePion)
    {
        EtatCasePion = newEtatCasePion;     
    }
    
//    Les Getters de chacune des methodes de Set pour gerer les cases à pion
    
    public int getEtatCasePion()				//Recuperation de l'etat de la  case
    {
        return EtatCasePion;
    }
    
    public char getPionPosition() 				//Recuperation de la Colonne de la case
    {
      return PionPosition;      
    }

    public int getLignePion() 					//Recuperation de la ligne de la case
    {
      return LignePion;
    }
   
    
}