
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public final class Puissance4 extends JFrame 
{ 
    
    int size;                            
    int OrdreJoueur=0;                   
    int NbreDeJoueur;                   
    static int NbreDeCsePleine=0;     
    String J1,J2 , Col1,Col2;
    int taille;
    JPanel PanInfo;
    ImageIcon ColJoueur1;
    ImageIcon ColJoueur2;
    ImageIcon vide;
    JLabel Info;
   
    JFrame frame;                   
    JPanel panel;                   
    JButton[][] buttons;          
    CasePion FenetreDeJeu[][];                   
    GridLayout grid;            
  
   
    
//    Constructeur de l'inteface de jeu et sa presentation
    
    public Puissance4 (String Col1,String Col2,String J1,String J2,int  taille) 
    {       
    	this.taille = taille;
    	this.J1=J1; this.J2=J2; this.Col1=Col1; this.Col2=Col2; 
    	
//    COnstruction de la Frame
        frame = new JFrame("VOUS JOUER A PUISSANCE 4");
        panel = new JPanel();
        
        // Button icons
        this.vide   = new ImageIcon("");
        
        if(Col1 == "bleu") {
        	System.out.println("COULEUR 1 BLEU");
        	 ColJoueur1 = new ImageIcon("images/bleu.png");
        	 ColJoueur2 = new ImageIcon("images/rouge.png");
        	
        }
        if(Col1 == "rouge") {
        	System.out.println("COULEUR 1 ROUGE");
        	ColJoueur1 = new ImageIcon("images/rouge.png");
       	 ColJoueur2 = new ImageIcon("images/bleu.png");
        }
        	
//        	}
//        if(Col1 =="bleu") {
//        	ImageIcon Joueur1 = new ImageIcon("images/bleu.png");
//            ImageIcon Joueur2 = new ImageIcon("images/rouge.png");
//        }
         
        playerNumberAndTaillePlateau();  // Permettra d'initialiser le type de partie que nous voulions jouer
        dynamicAllocation();         // Permettra de creer le Plateau de Jeu dynamique

        buttons = new JButton[getTaillePlateau()][getTaillePlateau()];    // Creation d'un tableau de bouton avec des dimension qui lui seront communiqué
        grid = new GridLayout(getTaillePlateau(),getTaillePlateau());     // Initialisation d'un GridLayout avec les meme dimensions
        panel.setLayout(grid);   										  // Le panel qui contiendra le plateau de jeu cadrillé est ainsi créer
        
// Initialisation du Plateau 
        PlateauInitial();
        
//        Panneau Information 
        PanInfo = new JPanel();
        Info = new JLabel("Information du Jeu"+"\n");
        PanInfo.add(Info);

        
        frame.add(panel,BorderLayout.CENTER);
        frame.add(PanInfo,BorderLayout.PAGE_END);
        
// Récuperer la dimension de l'écran
        Dimension tailleMoniteur = Toolkit.getDefaultToolkit().getScreenSize();
        int longueur = tailleMoniteur.width * 2/3;
        int hauteur = tailleMoniteur.height * 2/3;
//régler la taille de JFrame à 2/3 la taille de l'écran
        frame.setSize(longueur, hauteur);
        frame.setLocationRelativeTo(null); // Centrage de la fenetre au centre de l'eecran
        frame.setVisible(true);            // le rendre visible
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
   
 //Methode qui gère la taille et le renvoie de la taille du Plateau
    
    public void setTaillePlateau(int newSize) // prends la taille en parametre d'entrée
    {	size = newSize;		}
    
    public int getTaillePlateau()
    {	return size; 	}
    
// Methode qui renvoie les cases Pleines ou deja utilisé
    
    public static int NberOfLivingCasePions()
    {	return NbreDeCsePleine;		}
    
// Initialisation du nombre de joueur Possible et de la taille du jeu souhaité
    
    public void playerNumberAndTaillePlateau()
     
    {	NbreDeJoueur  = 2; // Un Jeu a 2 Joueur
    
//    	String TailleSouhaite = JOptionPane.showInputDialog( " Taille du Plateau de Jeu " );
//    	int TaillePlateau = Integer.parseInt(F);
    int TaillePlateau = taille;	
    
    	if(TaillePlateau < 4)
        {
           JFrame frameInputError = new JFrame();
           JOptionPane.showMessageDialog(frameInputError,
           "La taille du plateau doit etre au minimum superieur ou egale à 4  !!",
           "Erreur Taille",
           JOptionPane.ERROR_MESSAGE);
           System.exit(0);
        }
    	
        setTaillePlateau(TaillePlateau);  // Le plateau pour La puissance4 est de 7 par 7
    }

// Creatiopn d'un plateau dynamique qui contiendra nos case a pion, dans lesquel nous pourrons mettre le boutons et manipuler la case 
    public void dynamicAllocation()
    {
        FenetreDeJeu = new CasePion[getTaillePlateau()][getTaillePlateau()];
        for (int i = 0; i <getTaillePlateau(); i++)
        {for (int j = 0; j <getTaillePlateau(); j++)
            {FenetreDeJeu [i][j]=new CasePion();
            }	}	} 

// Ajout des boutons au plateau 
     
    public void AjoutBoutonAuPlateau()
    {	for(int i=0; i<getTaillePlateau(); ++i) // parcourir les indices 
        {	for(int j=0; j<getTaillePlateau(); ++j) //parcourir les colonnes
            {	buttons[i][j] = new JButton(vide); // remplir avec chaque case pion avec un boutonqi sera de type vide
                buttons[i][j].addActionListener(new listenButtonTwoPlayers());  // Ajout d'une action sur chacun des boutons
                panel.add(buttons[i][j]);   // Ajout du bouton au panneau des grids
            }	}	}      
//Initialisation de toutes les CasePion à vide ceci en previson au Restart du jeu 
    public void PlateauInitial()
    {	for (int i=getTaillePlateau()-2; i>= 0; --i) // On boucle sur les indices des lignes du plateu moins 2 et on decremente
        {	for (int j = getTaillePlateau()-1; j>=0; --j) // On boucle sur les indices des colonnes du plateu moins 2 et on decremente
            {	FenetreDeJeu[i][j].setEtatCasePion(-99); // Pour chacune de ces case on met leur etat a -99 etat inactif
            }	}
        
       AjoutBoutonAuPlateau(); // Apres avoir mis toutes les cases a vide on reremplis par les boutons
    }
    
    
//Condition de Victoire
    
    public void JoueurGagnant(int Gagnant) // Ici gagnant represente l'etat est rempli d'une case et est ce que le coup jouer permet de gagner
    {	for(int i=0; i<getTaillePlateau(); ++i) // on boucle sur les ligne et indice 
        {	for(int j=0; j<getTaillePlateau(); ++j)
            {	
        		if(FenetreDeJeu[i][j].getEtatCasePion() == Gagnant) // et donc pour chacunes des case qui vient d'etre rempli ou qui est dans letat rempli; on fait 
            	
// Recherche de victoire en Position verticale
       {		if(i+3<getTaillePlateau()) // les limites du tableau en colonne sont 7 et on gagne a 4 alignés : si il ya 3 places en haut de la case
       {	//On verifie si le i+1 a i+3 sont aussi dans le meme etat Gagnant
    	   		if(FenetreDeJeu[i+1][j].getEtatCasePion() == Gagnant && FenetreDeJeu[i+2][j].getEtatCasePion() == Gagnant && FenetreDeJeu[i+3][j].getEtatCasePion() == Gagnant)  
       {	
    		    if(Gagnant==1) // Si Gagnant est associé a Joueur 1 alors l victoire reviens a Jouer 1
                        Affichage(1);
                else
                        Affichage(2);	// Sinon elle revient a Joueur 2
    		    }	} 
                    
// Recherche de Victoire en condition horizontale
      if(j + 3 <getTaillePlateau()) // limites en lignes egale 7 et meme raisonnement 
      { 
    	  		if(FenetreDeJeu[i][j+1].getEtatCasePion() == Gagnant && FenetreDeJeu[i][j+2].getEtatCasePion() == Gagnant && FenetreDeJeu[i][j+3].getEtatCasePion() == Gagnant)
              { 
                if(Gagnant==1)
                     	Affichage(1);
                else
                        Affichage(2);
                }	}

// Recherche de Victoire en Diagonale de Gauche vers la Droite
      if(i  < getTaillePlateau()- 3 && j<getTaillePlateau()-3) // en Diagonal donc on considere les deux positions ou coordonnées
      {
            	if(FenetreDeJeu[i+1][j+1].getEtatCasePion() == Gagnant && FenetreDeJeu[i+2][j+2].getEtatCasePion() == Gagnant && FenetreDeJeu[i+3][j+3].getEtatCasePion() == Gagnant)
                {  
                if(Gagnant==1)
                        Affichage(1);
                else
                        Affichage(2);
           }   }

// Recherche de Victoire en Diagonale de Droite vers la Gauche
       if(i  < getTaillePlateau()- 3 && j - 3 >= 0 )
        {
           		if(FenetreDeJeu[i+1][j-1].getEtatCasePion() == Gagnant && FenetreDeJeu[i+2][j-2].getEtatCasePion() == Gagnant && FenetreDeJeu[i+3][j-3].getEtatCasePion() == Gagnant)
                {
                if(Gagnant==1)
                        Affichage(1);
                else
                        Affichage(2);
              }		}     }      }       } 
    		} 
    
    
   
// Affichage du Vainqueur
   public void Affichage(int JoueurGagnant)
   {
       JFrame frameAffichage = new JFrame();       
       if(JoueurGagnant==1)
       {
            JOptionPane.showMessageDialog(frameAffichage,
            "\nGagnant : "+ J1 +" \n\n Un Nouveau jeu peut commencer.\n\n",
            "FIN DE LA PARTIE",
            JOptionPane.INFORMATION_MESSAGE);
            startAgain(); 
       }
       else
       {
            JOptionPane.showMessageDialog(frameAffichage,
            "\nGagnant : "+ J2 +"\n\n Un Nouveau jeu peut commencer.\n\n",
            "FIN DE LA PARTIE",
            JOptionPane.INFORMATION_MESSAGE); 
            startAgain();    
       }
   }
   
   
   public void warningMessage()
   {
       JFrame frameWarning = new JFrame();           
       JOptionPane.showMessageDialog(frameWarning,
       " Position non autorisé !!\n La CasePion n'est pas vide , Vous pouvez pas la remplir .", "Warning",
       JOptionPane.WARNING_MESSAGE);
   }
   
   
// ordre qui permet de Lancer ou de Relancer un jeu 
// On remet juste le code qui permet de faire afficher un plateau de jeu de type initial
   public void startAgain()
   {	for(int i=0; i<getTaillePlateau(); ++i)
        {	for(int j=0; j<getTaillePlateau(); ++j)
            {	FenetreDeJeu[i][j].setEtatCasePion(-99);  
                buttons[i][j].setIcon(vide);       
            }		}
        
        frame.setVisible(false);             					// On remet le frame avec cest nouvelle case a l'ecran
        new Puissance4 (Col1, Col2,J1,J2, taille) ;        			// Demarage d'un nouveau jeu
   }
     
  
// Gestion des celules libre et des CasePion qui peuvent se faire remplir 
    public void LiberationCaseAuDessus(int rowPos, int columnPos)
    {	try 
        		{	FenetreDeJeu[rowPos-1][columnPos].setEtatCasePion(0);    }   
        catch 	(Exception ex) { }      
    }
     

//Remplissage des cases et deplacement du curseur
    public void moveComputer(int LignePion)
    {	int l,m;
        boolean flag=false;
        for(l=getTaillePlateau()-1; (l>=0)&& !flag; --l)
        {	for(m=0; (m<getTaillePlateau()) && !flag; ++m)
        {		if(FenetreDeJeu[l][m].getEtatCasePion() == 0) // la case n'a pas encore été rempli alors 
        {
                buttons[l][m].setIcon(ColJoueur2);         				// On lui donne l'image du joueur 2
                FenetreDeJeu[l][m].setAllPosition('O', LignePion); 		// chargement des parametres de coordonnées de la nouvellecase remplie
                FenetreDeJeu[l][m].setEtatCasePion(2);        			// Changement de l'etat de la case
                ++NbreDeCsePleine; 										// Le nombre de case rempli s'incremente
                JoueurGagnant(2); 										// Verification du gagnant
                flag = true;  
                LiberationCaseAuDessus(l,m);
         }		}		}		}


// Recuperation des clic sur les boutons

    private class listenButtonTwoPlayers implements ActionListener
    { @Override
        public void actionPerformed(ActionEvent e)
        {	
    		try 
        {		int eventFlag = 0;
                int flagOrdreJoueur=0;

            for(int i=getTaillePlateau()-1; i>=0; --i)
            {	for(int j=0; j<=getTaillePlateau()-1; ++j)
            		{		if(eventFlag==0 && buttons[i][j]== e.getSource()) // Recuperation des informations du bouton cliqué
        					{  		if(flagOrdreJoueur==0 && OrdreJoueur%2==0) 
        { 
  // Traitement des Actions du Joueur 1  
                for(int k=0; k<=getTaillePlateau(); ++i)    
        {
                if(FenetreDeJeu[i-k][j].getEtatCasePion()==0 && OrdreJoueur%2==0)
        {
                    buttons[i-k][j].setIcon(ColJoueur1);          			// Assignation d'une couleur au Joueur 1 
                    FenetreDeJeu[i-k][j].setAllPosition('X', i);     	// Assination des parametres de position
                    FenetreDeJeu[i-k][j].setEtatCasePion(1);  			// Assignation de l'etat rempli correspondnat a Joueur1
                    ++NbreDeCsePleine;  								// On incrmente lenombre de case pion pleine
                    JoueurGagnant(1);     								// A chaque coup jouer et les modification effectuer on verifie la victoire
                    flagOrdreJoueur=1;   
                    eventFlag=1;
                    break; 
        }	}

                	LiberationCaseAuDessus(i,j);   					// Liberation de la case audessus de la où vient de jouer le joueur 1
                	PanInfo.add(new JLabel("..."+J2+" à vous de jouer ... "+"\n"));
                	++OrdreJoueur; 										// On accorde la main de jeu a joueur 2
                    break;
                            }

  // Traitement des Actions du Joueur 2
               if(flagOrdreJoueur==0 && OrdreJoueur%2==1) 				// Verification des conditions qui permette a Joueur 2 de jouer
         { 
               for(int k=0; k<=getTaillePlateau(); ++i)					// Parcourir les case du plateau
         {
               if(FenetreDeJeu[i-k][j].getEtatCasePion()==0 && OrdreJoueur%2==1)    //  Verifiaction de l'etat des case où J2 veut jouer
         {
            	   	buttons[i-k][j].setIcon(ColJoueur2);           		 // Assignation d'une couleur au Joueur 2
            	   	FenetreDeJeu[i-k][j].setAllPosition('O', i);    	 // Assination des parametres de position   
                    FenetreDeJeu[i-k][j].setEtatCasePion(2);             //	Assignation de l'etat rempli correspondnat a Joueur1
                    ++NbreDeCsePleine;
                    JoueurGagnant(2);
                    flagOrdreJoueur=1;
                    eventFlag=1;
                    break;
         }	}
                    LiberationCaseAuDessus(i,j);
                    PanInfo.add(new JLabel("..."+J1+" à vous de jouer ... "+"\n"));                 
                    ++OrdreJoueur;
                    break;
         }		}		}		}		}
            
               catch(Exception ex) 
            		{ warningMessage(); }     
       
        }		}	
    
    
	}



